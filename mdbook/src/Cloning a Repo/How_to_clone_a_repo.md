# How to clone a repository from gitlab

*Note: For notes on creating an issue and editing see section below

On your system create a directory in an area you would like to work:

![](assets/create_directory.png) 



In gitlab find the directory you want to clone.
click on the clipbard and it will copy the https path to the repo.

*Note: You may have to actually highlight and copy for this to work, sometimes it gets picky if it won't copy using the clipboard icon.


![](assets/clipboard_icon.png)


On your system, go into the directory you created

![](assets/change_directory.png)

Then type: "git clone" and then right click and paste what was saved in the clipboard

![](assets/copy_paste_repourl.png)

If it works, it will look something like this:

![](assets/clone_working.png)

Then type "dir" to get a list of directories that were pulled from the repo.

Use the cd command to go into the directory you want to work with

![](assets/change_to_directory.png)

To work with all the files in code you can enter "code ."

or

go into a specific directory and then type "code ."
"code ." will bring the files up in visual studio

![](assets/code_visual_studio.png)

---

# Create an issue and edit

To create an issue and edit after you have cloned a repo:

Go into gitlab and to the repo you want to make a change to.

On the left hand side, click on the icon that looks like a page with a tab.
Then click on "List" in the drop down menu that appears

![](assets/issue_list.png)

A new screen will pop up

On the right will be a blue button that says "New Issue"

Click on it

![](assets/new_issue_button.png)

A new page will come up titled "New Issue"

Fill in the Title

Add a description

Assign to yourself by clicking on "Assign to me"

You should then be able to click on "Create Issue" on the bottom left of the screen

![](assets/assign_issue.png)

A new page will pop up with the title of the issue you created.

Click on the blue "Create merge request"

![](assets/create_request_button.png)

Another page will come up that is a draft of the request

Find the white button that says check out branch and click on it

![](assets/check_out_branch.png)

A pop up window will show up giving you a bunch of git commands

You will want the top one.  Copy these commands to paste in the next step.

*Note: It may be a good idea to copy them into a text doc in case something happens and you lose the branch info and are not skilled in git commands.

![](assets/branch_commands.png)

Go back into your terminal

Make sure you are at a place in the repo you cloned that you want to work in.
Usually in the base of your repo or directory that you made earlier

![](assets/identify_directory.png)

If you have created a branch previously, then it is a good idea to run
the command "git branch" to check and see what branch you are on.

![](assets/identify_branch.png)

You do not want to make changes to master as a rule.
Changes should be done on branches then pushed up to get and then merged.

If you are not on master when you run this command, it is a good idea
to run the command "git checkout master".

After, verify you are in master by again running the command "git branch"

It should be in the master branch.

Run the command "git pull" to make sure that you are working with the most current version of the repository.

![](assets/git_pull_to_master.png)

Now that the repo is up to date, create the branch on your system by using the commands we received from gitlab.

Either paste the commands in or type them into your terminal

![](assets/create_branch_locally.png)

You should now be in a new branch

Run the command "git branch" to verify

To open the folder and work in visual studio, type in "code ."
 

![](assets/open_vs_code.png)


This will bring up a visual studio session of your folder for you to make changes on.

Go in and work onthe files you want to edit or create, or create more directories depending on your needs.

Once done you will save your work in visual studio

Go back into your terminal

Run the command "git status" to check that there is something to be added to git (This can be done directly from the visual studio terminal as well)


![](assets/git_status.png)

This will tell you what files were changed 

To push files up to git you will need to add them.

You can add them all at once with the command "git add ."

or you can add them individually if you don't wnat to add all of them with the same command only with the name of the file you want to add
    "git add example_file.txt"

![](assets/git_add.png)


Once the files are added we need to commit them before pushing them to git

Run the command 
    git commit -m "something meaningful about the work you are commiting"

![](assets/git_commit_command.png)

Now that the files are added and commited we can push them to git

Run the command

    git push

*Note: If you are no longer needing to make changes on this branch it is a good idea to return to the master branch, go to the base of your directory if you are working in sub directories to make things less confusing before running this command
    
    git checkout master

*Note: once you leave the branch and return to master, any changes will no longer be visible unless you return to the branch you were working on.  Once the files are merged later, as explained below you can run the command "git pull" and it will change your files locally to the current version and changes

Now that the changes have been added, committed and pushed to git, we can go back into gitlab.

If you are no longer on the "Draft" page of your issue you will need to return to it

Otherwise refresh the page and your request to merge should come up

![](assets/merge_draft_page.png)

Mark as ready with the blue and white button in the upper left.

or

further down on the page there is another "Mark as ready" to click.

If you need someone else to make the merge because you do not have permission you can edit the assignee on the left and it should notify them, otherwise let someone with permissions to merge know.

If you are able to merge and you are confident you do not need it to be looked over then you can click on the merge button

![](assets/request_merge_and_assigne.png)

It may take a while, but once it has been merged you can go back into your terminal, make sure you are on the maste branch in your file structure and run the command

    git pull

to make the files change locally on your system. 
