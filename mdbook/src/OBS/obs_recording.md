# OBS General Information

![image](assets/obs_logo.png)

OBS, as seen in the logo above is short for the "Open Broadcaster Software". It is a very powerful software tool for manipulating audio/video sources while streaming and/or recording. The software is available on most operating systems (Windows/Linux/Mac) and should share a very similar UI/experience. All the sections following will be using a Windows 10 system, but should be easily followed on other operating systems.

|Description|Link|
|-|-|
|Direct Downaload| https://obsproject.com/download |
|Github Project|https://github.com/obsproject/obs-studio  |
|Wiki/Other Guides| https://obsproject.com/wiki/ |

Below is a basic screenshot pointing out the most used sections/buttons for recording with OBS.  

![image](assets/obs_overview.png)  

## Installing OBS

Rather than recreate the wheel, follow the steps from the sources installation instruction. Windows is simply downloading a pre-built windows installation package.  

Download Section: https://obsproject.com/download  
Installation Instructions: https://obsproject.com/wiki/install-instructions  

## Recording w/ OBS  

This section will go over the absolute bare minimum required to start recording a Screen/Window on your computer.  

**Additional Tools/Guides**  
Quickstart Guide: https://obsproject.com/wiki/OBS-Studio-Quickstart  
Overview Guide: https://obsproject.com/wiki/OBS-Studio-Overview  
Source Guide: https://obsproject.com/wiki/Sources-Guide  

Once OBS is installed and running, the main screen has sections that need to be setup. The "Scenes" and "Sources" sections are likely blank (if freshly installed) and need to be configured.  

# Setting up a scene

Scenes are simply a named set of sources that can be switched while streaming/recording. Scenes are a sort of "snapshot" of position windows/sources that you can setup and change to as needed.  

You can have a scene that is setup just to record a single display (if you have multiple monitors/desktops) or application (like Discord or Microsoft Teams). That will be discussed more in the Scene setup section following this.  

- Click the `+` sign at the lower left corner of the "Scenes" box  

![image](assets/obs_scene_add_button.png)

- Input Scene description in the "Add Scene" dialog box  

![image](assets/obs_add_scene_diag.png)

- Click OK  

- Select the newly created Scene by clicking on the name in the Scenes box  

- You should see an "empty" Sources box directly to the right of the Scenes box that looks like this:  

![image](assets/obs_sources_box.png)

# Setting up sources

Sources allow you to layer multiple inputs into a single scene. You can put your webcam as an overlay to your main screen. You can also find an eye shaped icon ( ![image](assets/obs_visable_icon.png) ) next to each source to show/hide sources as needed (example: you have a whiteboard app or you want to switch between multiple windows easily).

- Click the `+` icon at the bottom left of the "Sources" box
- Select the source you would like to add:  
  - **Webcams**, select `Video Capture Device`  
  - **Entire displays** (Monitors), select `Display Capture`  
  - **Specific Applications**, select `Window Capture`  
      
*Note: Play around with the various options to see what all can be done. It is extremely powerful if you spend the time setting it up. Such as **Medis Source** allows for streaming a local audio/video file or you can add text to your sources with the **Text (GDI+)** option and have a fixed text put somewhere on your screen. Same for the **image** option... you could put a 39th Det 1 logo at the bottom corner if you wanted. The world is your oyster!*  

- Input a `name` in the `Create new` dialog box  

![image](assets/obs_create_source_diag.png)

- Click `OK`  
- Configure the properties for your new source and click OK  
*Example of `Window Capture` where I have chosen `discord.exe` as the window*  
**The `Window Capture` will record ONLY that window. Even if you move other windows over it, it won't show it. Also, the size of the window can be seen/modified. I would recommend playing around with this feature if you have the time.**  

![image](assets/obs_properties_box.png)    

- **Optional**: to modify/return to properties, double click on the newly added name found in the Sources box  
- **Optional**: Repeat the bove steps to add additional sources  

What was likely previously a black box should now show your source inside it. You can click and drag them around to modify their position. The black box/preview panel is exactly how the video will record, so attempt to use up as much space as possible.

- If you have sources that aren't used all the time, you can use the ![image](assets/obs_visable_icon.png icon to turn them on/off  
- If you want to `lock` in a window, you can use the lock icon next to the eye to hold something in place  
- To position windows, you can click/drag them around and resize them using the red boarder  
- To move a source forward/backwards, right click on the source (either in the Sources box or directly on the source in the preview) and select `order` then whatever diretcion you want that source to move  

# Start/Stop recording

Once your video scene/sources have been setup, you can click the `start recording` button and it will begin to record. 

*By default (in Windows) the videos will be stored in the Videos file under your user profile: `C:\Users\<username>\Videos`*

They will default to an MKV file. This can be changed in `Settings -> Output -> Recording Format` to other formats `(flv, mp4, mov, mkv, ts, m3u8)`

# Uploading/Distributing Videos

TBD... off site/commercial access storage is not currently setup. If you have an educational google account (prior students often count) that can be used as a temp storage.

|Service Name | File Size/Storage Capacity | Link | Comments/limitations |
|-|-|-|-|
|Google Drive|15 GB | https://drive.google.com/ | potentially "unlimited" with edu accounts |
|Mega|15GB|https://mega.io/| Possibly 50GB under some circumstances |
|Media Fire| 1 GB |https://www.mediafire.com/| Download/speed limited |

Other potential solutions: https://en.wikipedia.org/wiki/Comparison_of_file_hosting_services

