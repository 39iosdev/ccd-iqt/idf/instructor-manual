# How to Grade a Performance Test

** Link is to all the grading rubric **

1. Go into the students repo, where they should have created a merge request for the test upon completion of the test.

2. Do a quick scan over the students code to check for any hard coded solutions to pass the unit tests. Also the students should have also submitted a screenshot of the unit tests results, browse over that as well.

3. Copy the students code into your testing environment, into the the testfile, to further test the solutions.

4. Run the unit tests for the first step of grading. 
    - Python
    ```bash
    python3 runtests.py
    ```

    - C
    ```bash
    gcc -o test runtest.c testfile.c
    ./test
    ```

5. If all of the unit test pass, further look through the code to ensure the student didnt hard code unit test solutions, and further evaluate the code.

6. Input any findings onto the Rubric 
    - Input the Students name on line 2
    - Each individual question will have its own grade, enter that grade in the correct fields 
    - If all unit test pass and there is no descrepency in the students solution no further action taken
    - If the student fails to pass all unit test, test the code further to see if there are any partial credit items that the student can earn
    - Insert the results into the proper sections on the rubric to explain why any deduction happened, or a brief explanation for any failed unit test


7. In the students merge request via gitlab, copy the rubric into the comment section of the merge request, that way the students can review the results and any needed updates that they need to make are all on the merge request for them.

8. Keep a copy of every students rubric and send them to the correct person to update the StanEval master grading report. 
