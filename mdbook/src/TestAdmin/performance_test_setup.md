# How to Create a Performance Test

```
https://gitlab.com/39iosdev/ccd-iqt/idf/evaluations/-/archive/master/evaluations-master.zip
```
**Zip contains all evaluations  **
  - "C Programming"
  - "Networking" (Contains Advanced Python)
  - "Python" (Basic)

**All evaluations located here:**
```
https://gitlab.com/39iosdev/ccd-iqt/idf/evaluations
```

## Option 1: Zip download

1. Download master evaluation zip above
2. Extract specific evaluation folder
   (i.e. "evaluations-master/python/performance/python_basics_quiz_a")
3. Remove any non *py files/directories
   * __pycache__ folders / pyc files
   * yml/json/xml files
4. Zip folder up for distribution through Discord (or choice upload method)
5. Have students create new branch/issue/merge request to upload files and submit solutions



## Option 2: Git/Clone

SSH
```
git@gitlab.com:39iosdev/ccd-iqt/idf/evaluations.git
```
HTTPS:
```
https://gitlab.com/39iosdev/ccd-iqt/idf/evaluations.git
```

1. Clone Repo (if not already done)

```
git clone https://gitlab.com/39iosdev/ccd-iqt/idf/evaluations.git
```

2. Navigate to directory of test to administer
  - "C Programming"
  - "Networking" for Advanced Python/Networking combined
  - "Python" for Basic Python

**Example dir**
```
<git clone location>\evaluations\python\performance\python_basics_quiz_a
```

3. Performance folders should contain seperate folders for each problem

**Python basic example**
```
12/31/1999  11:59 PM    <DIR>          .
12/31/1999  11:59 PM    <DIR>          ..
12/31/1999  11:59 PM    <DIR>          perfect_num
12/31/1999  11:59 PM    <DIR>          rectangle
```

4. Zip those folders together using whatever method you prefer. Command line arguements below

```bash
# Windows command to zip all files in folder
powershell "Compress-Archive * test.zip"

# Linux (untested... but should work)
zip -r test.zip .
```

5. The student will have to use the given runtest.py / runtest.c file that has the unit tests for the given test. 
  - Python
```bash
# Students will have to run the unit test file to properly test thier scripts
python3 runtest.py
```

  - C
```bash
# The student will first have to compile/link thier code to the runtest.c testing file that contains the unit test
gcc -o test runtest.c testfile.c
./test
```

6. Have students create new branch/issue/merge request to upload files and submit solutions
