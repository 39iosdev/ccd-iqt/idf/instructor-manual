# Knowledge Tests & Google Forms

**If you can't access the links below, work with another instructor who can grant those permissions to you**

*Still need C/Assembly source questions added and A/B tests created*

## Pre-defined Tests (Should be used in most cases)

**(A) *Python* Basics Knowledge Test**
```
https://docs.google.com/forms/d/1yJJh5700sjIm8nnIpGTp9hZl_YkuZR6I_jaCinXSiPw
```

**(B) *Python* Basics Knowledge Test**
```
https://docs.google.com/forms/d/1JLxu3XddFYHksdEtXeyOBTEgcZ56sPJ7xYFt4q7JVdk/
```

## All Questions (Used if you need to create a new test or replace questions)

***Python* Basics Knowledge Question Bank/Pre-defined tests above:**
```
https://docs.google.com/forms/d/1HwdK9t_sGG8MuxECwBiDvkrhAkUHQtjtC72pDGA4YWA/
```

---

**<p style="color:red;text-align:center;font-size:20px">Before you begin**  
Verify you have access to the question bank(s)/test(s)  
you need to evaluate on by visiting the links above.</p>  

<img src="assets/request_edit.png" style="display:block;margin:0 auto">

If you see the "Request edit access" button at the bottom right of your window then you do not have permissions setup for the account you are currently logged in with. You either need to log in to the correct account or request access from one of the instructors that can modify the permissions to the test bank.  