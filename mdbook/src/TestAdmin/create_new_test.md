## Creating a new knowledge test form (Use steps 1-9 if an A/B test does **NOT** already exist)

1. Navigate to:
    ```
    https://docs.google.com/forms/
    ```
2. If you have navigated to either of the forms listed above, you should see them in your recent forms section

    ![image](assets/forms_main_view.png)

    If you don't see any forms (like the image below) then you are lacking permissions or you are logged in to the incorrect account.  
      
    ![image](assets/forms_missing.png)



3. Create a new form by either clicking the "Blank"/Plus sign box  
   (if you see a "Start a new form" template gallary like shown above)
   
   If you don't see the large box, you should see a small round button at the bottom right hand corner.

    Highlight over the:    
    ![image](assets/google_plus_button.png)  
    button and then click   
    ![image](assets/create_new_form_button.png)  
    on "Create new form"


4. Change the name "Untitled form" to a new descriptive name:  
   *(e.g. "IDF Class 21-02 Python Basics")*  
    ![image](assets/form_name.png)

## Import Questions:  

5. Click the "import questions" button. It can be found on the side menu (or at the bottom if your window is too narrow).  
    ![image](assets/import_questions_side.png)
    ![image](assets/import_questions_bottom.png)

6. Select the appropriate question bank from the "Select Form" box then click **Select** You can change the display from 'Grid view' to 'List view' if you can't see the form names properly.
   
   ![image](assets/select_form.png)

7. New forms will auto populate your first blank question. Deleted this by clicking on the garbage can in the question box. If you don't see the can, click anywhere in the question box to expand.

    ![image](assets/emtpy_question.png)

8. You should now see your form with a menu on the right hand side, showing the questions to import. Select all the questions you would like to add to your quiz, then click import questions.  

    ![image](assets/import_question.png)

9.  Now you can verify the quiz/questions are all there and that your form has a name. You can also modify the theme, add an image to the header and even have the quiz randomize order of questions each time.
   
    - **Randomize order** by clicking the cog at the top right, selecting the Presentation tab, then checking the "Shuffle question order" checkbox. Click save to store the setting.
    ![image](assets/randomize_order.png)

    - **Customize theme** by clicking on the paint palatte icon at the top right. Here you can set a Header Image to distinguish your test/content from others and then choose a theme color/background.  
    ![image](assets/customize_theme.png)
