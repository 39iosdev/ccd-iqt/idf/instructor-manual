## Administer Test to Students

1. Navigate to google forms and open the test you would like to copy/administer
```
https://docs.google.com/forms/
```  
![image](assets/select_test.png)  

2. Click the vertical 3 dots at the top right, then select Make a copy  
   ![image](assets/make_copy.png)

3. Give the copy a descriptive name *(e.g. "IDF Class 21-02 Python Basics")*. The folder should be set to My Drive. Then click **OK**. You should see a new tab/window open with the copy of your test.  
   ![image](assets/make_copy_name.png)

4. Click on Send at the top right  
    ![image](assets/send_button.png)  

5. There should be a send form box on screen, that defaults to the email tab.
   
   - Select the "Send via" link icon/tab ![image](assets/link_icon.png)  

   - Collect emails should be checked  
   
   - (Optional) Can check the "Shorten URL" to get a easier to read/input link
   
   - Then click "Copy" to copy the URL to your clipboard  
   ![image](assets/send_options.png)

6. Once the URL is copied, you can close the box by click "Cancel"

7. Prior to administering the test, go over the performance evaluation form with the students and have them sign/return.


   - [Python Basic Knowledge Quiz](https://gitlab.com/39iosdev/ccd-iqt/idf/python/-/blob/master/mdbook/src/Basic_exam_student_docs/C-4198AE_-_Student_Responsibilities.pdf)
   - [Python Basic Performance Evaluation](https://gitlab.com/39iosdev/ccd-iqt/idf/python/-/blob/master/mdbook/src/Basic_exam_student_docs/C-4198ME_-_Student_Responsibilities.pdf)
   - [Python Advanced/Networking Knowledge Quiz](https://gitlab.com/39iosdev/ccd-iqt/idf/python/-/blob/master/mdbook/src/Advanced_exam_student_docs/C-4199AE_-_Student_Responsibilities.pdf)
   - [Python Advanced/Networking Performance Evaluation](https://gitlab.com/39iosdev/ccd-iqt/idf/python/-/blob/master/mdbook/src/Advanced_exam_student_docs/C-4199ME_-_Student_Responsibilities.pdf)
   - [C Programming (Basic) Knowledge Quiz](https://gitlab.com/39iosdev/ccd-iqt/idf/C-Programming/-/blob/master/mdbook/src/Basic_exam_student_documents/C-4398AE_-_Student_Responsibilities.pdf)
   - [C Programming (Basic) Performance Evaluation](https://gitlab.com/39iosdev/ccd-iqt/idf/C-Programming/-/blob/master/mdbook/src/Basic_exam_student_documents/C-4398ME_-_Student_Responsibilities.pdf)
   - [C Programming (Advance) Knowledge Quiz](https://gitlab.com/39iosdev/ccd-iqt/idf/C-Programming/-/blob/master/mdbook/src/Advanced_exam_student_documents/C-4399AE_-_Student_Responsibilities.pdf)
   - [C Programming (Advance) Performance Evaluation](https://gitlab.com/39iosdev/ccd-iqt/idf/C-Programming/-/blob/master/mdbook/src/Advanced_exam_student_documents/C-4399ME_-_Student_Responsibilities.pdf)

8. Once you have recieved the signed Quiz/Eval form back from all the students, you can note the start time and give the URL out to the students to take the test.

**<p style="color:red;weight;text-align:center">IMPORTANT:  
Ensure students do NOT use .mil email address!  
It may get flagged as spam and possibly never reach their inbox.</p>**

9. Now you can see the results in the "Responses" tab of your copies test instance. Initially it will show empty like this:  
    ![image](assets/response_tab.png)

    But will start populating as students complete the test like this:
    ![image](assets/response_tab_filled.png)

10. (Optional) Once the test is complete/time has elapsed, you can close the test, under the responses tab by unchecking the "Accepting responses" button. This is optional, but stops any further access to the questions/ability to submit responses.  

11. Once everyone is complete, you can review all the tests and release them individually or you can release all of them at once.
   - If you don't have the form/quiz open, open it and click the "Responses" tab at the top (it should show a number of how many people have submitted a response)
   - Click on the "Indivdual" tab
   - Click "Release Score", which should bring up a popup
   - Using the checkboxes, check all the tests you want to release then click the "Send Emails and Release" button
   - All the students should get an email with a link to review their right/wrong answers and their total score

12. Once everyone has access, you can then review the questions as a class using the "Summary" tab. It shows the average/median/range of scores, frequently missed questions, scores for all members who completed it and then a breakdown of responses and how they were answered.


