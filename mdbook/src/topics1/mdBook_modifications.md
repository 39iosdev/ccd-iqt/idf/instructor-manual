## Overall material content structure

The sections following this overview will break down each portion further, but the overall structure for the current IDF material is as follows:

GitLab environment:

**Main course URL:** https://gitlab.com/39iosdev/ccd-iqt/idf/

Here you will find all the individual sections/projects within GitLab (all our mdBook, slides, images, etc).

![image](assets/IDF_GitLab_main_page.png)

Each section contains the course ciriculum and associated files for building the mdBook/Slides.


## Viewing mdBook Content

The generated/viewable mdBook content can be found in the following location:
* https://39iosdev.gitlab.io/ccd-iqt/idf/python/
* https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/
* https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/
* https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/
* https://39iosdev.gitlab.io/ccd-iqt/idf/agile/
* https://39iosdev.gitlab.io/ccd-iqt/idf/pseudocode
* https://39iosdev.gitlab.io/ccd-iqt/idf/network-programming/

You can see that there is a root path (```https://39iosdev.gitlab.io/ccd-iqt/idf/```) for everything and the topic name (```python```, ```C-Programing```, etc) is the only difference between them all. These links can also be found on the top/header of each respective section in the gitlab environment.

**Example path:**
https://gitlab.com/39iosdev/ccd-iqt/idf/python

**Red box shows where in the project header the link can be found:**  
![image](assets/mdbook_link_location.png)

## Viewing Reveal.js slides

Slide location/links can be found in their respective sections. Just like the mdBook sections, slide locations follow the same scheme with the addition of ```/slides/``` at the end of the contents URL.

Examples:
* https://39iosdev.gitlab.io/ccd-iqt/idf/python/slides/
* https://39iosdev.gitlab.io/ccd-iqt/idf/C-Programming/slides/
* https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/slides/
* https://39iosdev.gitlab.io/ccd-iqt/idf/assembly/slides/
* https://39iosdev.gitlab.io/ccd-iqt/idf/agile/slides/
* https://39iosdev.gitlab.io/ccd-iqt/idf/pseudocode/slides/
* https://39iosdev.gitlab.io/ccd-iqt/idf/network-programming/slides/
