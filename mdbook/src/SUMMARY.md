# Guide - Table of contents

[Introduction](README.md)

# mdBook/Slide modifications
- [mdBook/Slide modifications](topics1/mdBook_modifications.md)

# Video Recording
- [OBS Recording](OBS/obs_recording.md)

# Uploading Recordings
- [YouTube Recording](YouTube/youtube_recording.md)

# Knowledge Tests
- [Links and Access](TestAdmin/test_links.md)
- [Creating New Test](TestAdmin/create_new_test.md)
- [Administer Existing Test (A/B)](TestAdmin/administer_test.md)

# Performance Tests
- [Preparing Test](TestAdmin/performance_test_setup.md)
- [Grading Test](TestAdmin/grading_performance_exams.md)

