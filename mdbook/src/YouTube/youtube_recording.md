# YouTube General Information

All recording, uploading and streaming for YouTube requires that you have a valid user account. It is possible to use your personal account or you can create a seperate "business" account specifically for your course. YouTube requires an email address to create an account, but that can be created for free with gmail.

Example for the IDF course:  
**Email:** 39ios.idf@gmail.com  
**YouTube:** https://www.youtube.com/channel/UCvqhF1xi5dmuUIUNE96o70g  

*The YouTube address can potentially be customized if you do enough pre-requisites in the account to allow it. Most notably, you have to have 100 subscribers*

---

# Uploading Video to YouTube
**Requires YouTube account**

- Login to https://studio.youtube.com/  
[![image](assets/youtube_studio.png)](https://studio.youtube.com/)  
<br/>
- Click the ```CREATE``` then ```Upload Videos``` at the top right  
![image](assets/create_upload.png)  
<br/>
- Either ```drag/drop the video file``` into the window **or** click ```SELECT FILES``` and navigate to them on your computer then open them  
![image](assets/drag_drop_selectfiles.png)  
<br/>
- Modify the details of the video. Recommended options below:  
    1. **Title** (Defaults to file name)  
    2. **Description** (optional)  
    3. **Playlist** (Recommend creating a playlist for your class/course and adding them to a single spot)  
    4. Check if it's made for kids or not under **Audience**  

![image](assets/setup_video1.png)  
- Click ```NEXT``` at the bottom right **2 times** (to get to the Visability section  
<br/>
- Check ```Unlisted``` for visibility. *
*You can do Private as well, which requires every users email address to gain access*
![image](assets/visability.png)  

- Click ```SAVE```  
- You should then be given a link for your video  
![image](assets/video_published.png)  

---

# Setup Streaming 

**One Time / potentially 24 hour wait on initial setup**  
![image](assets/wait24.png)  
<br/>
If you have not completed the setup steps with a phone number that has already been verified to stream on YouTube, you will likely have to wait 24 hours before your account is processed to allow for live streaming. Another potential error message you might run into, related to the 24 hour wait period, will say that live streaming is not available right now.  
<br/>
![image](assets/live_stream_unavail.png)  
<br/>
**You can upload without this verification**.

## Steps to Setup Streaming
- Login to https://studio.youtube.com/  
[![image](assets/youtube_studio.png)](https://studio.youtube.com/)  
<br/>
- Click ```Content``` on left bar  
![image](assets/content_menu_button.png)
<br/>
- Click ```Live``` tab under ```Channel content```
- Click ```GET STARTED``` in the middle of the screen  
![image](assets/get_started_streaming.png)
<br/>
- Click ```ENABLE``` 
- Click ```Verify```
- Input phone number (options available to use a desk/work phone if you don't want to use a cell number) into ```What is your phone number?```
- Click ```GET CODE```
- Type in the 6-digit verification code recieved through SMS (or a voice/phone call) into the textbox
- Click ```Submit```

---

# Setting up Streaming to YouTube

Streaming directly to YouTube, provides the ability to use higher resolutions (Discord only allows for 720 under a free account). There are options for latency. However, even at the lowest latency setting, you still have a second *or more* delay. It might be possible to run a live class using only YouTube, but it will have a few second delay, in many cases, that will need to be considered.

*One benfit to streaming directly to YouTube (even if you won't be using it in a 'live' sense), is you can instantly save/setup your video on completion (no need to upload after it is done).*

## Setting up OBS/YouTube
- Login to https://studio.youtube.com/  
[![image](assets/youtube_studio.png)](https://studio.youtube.com/)   
- To **start a new live recording**, click the ```CREATE``` button at the top right, then click ```Go Live```
![image](assets/go_live.png)

    *If this is the first time you are setting this up/using it, you might see the following error that requires you to wait 24 hours from the time of setup to start streaming*  
    ![image](assets/wait24.png)
- The screen has 3 important sections to pay attention to:  
    1. The current streaming video
    2. The ```Stream key``` used to stream in OBS
    3. Latency. Use ```Ultra low-latency``` if you plan to allow students to view live and provide feedback/communicate. Use ```Normal latency``` if you are just using this as a recording method
    ![image](assets/stream_screen_key.png)
- **In OBS** click ```Settings``` at the bottom right
- **In OBS** click ```Stream``` on the left menu
- **In OBS** copy/paste your ```Stream key``` from YouTube into the ```Stream key``` box in the OBS settings window
- **In OBS** click ```Apply``` and ```OK``` to close the window  

*NOTE: OBS is now capable to streaming to your YouTube channel directly. However, you still have to make sure YouTube streaming is activated each time you want to record*  


# Starting a Live Stream with OBS/YouTube

- Login to https://studio.youtube.com/  
[![image](assets/youtube_studio.png)](https://studio.youtube.com/)  
- To **start a new live recording**, click the ```CREATE``` button at the top right, then click ```Go Live```
![image](assets/go_live.png)
- **In OBS** ensure your audio/video settings are correct, then click ```Start Streaming```

*If you have all your settings correct in OBS and YouTube is set to live stream, you should now be able to see*

- **On YouTube/Studio website** You should be able to see your stream live (1) and some basic information on analytics and stream settings  
![image](assets/active_stream.png)
<br/>
- **In OBS** you can stop your stream at any time, but clicking ```Stop Streaming```
<br/>
- **On YouTube/Studio website** you should then see a ```Stream Finished``` popup that shows some basic information about your session. Here you can click ```EDIT IN STUDIO``` to instantly save your video without any need to upload anything.
![image](assets/end_stream.png)  

*In the editing studio portion, you can trim your video and modify any settings you may want before saving it as a new video on your channel*

- **At minimum**, click on ```Details``` on the left pane and ensure all your settings are correct (Titel, Description, Audience, Visability)
<br/>
- Optional: Click the ```Select``` dropdown box under Playlists. Here you can add your video to an existing playlist (checking the box next to the playlist name) or create a new one by clicking the ```NEW PLAYLIST``` button at the bottom left of the dropdown
![image](assets/add_playlist.png)
